import os

import psutil
import pyudev


class FlashCardManager(object):

    def __init__(self, device_path):
        self.device_path = device_path
        self.context = pyudev.Context()

    def mount_and_read(self):
        files = []
        removable = [dev for dev in self.context.list_devices(subsystem='block', DEVTYPE='disk') if
                     dev.attributes.asstring('removable') == "1"]
        if removable and [dev for dev in removable if dev.get("DEVPATH").startswith(self.device_path)]:
            for device in removable:
                if device.get("DEVPATH").startswith(self.device_path):
                    partitions = [device.device_node for device in
                                  self.context.list_devices(subsystem='block', DEVTYPE='partition', parent=device)]
                    for p in psutil.disk_partitions():
                        if p.device in partitions:
                            files.append(os.listdir(p.mountpoint))
            if files:
                return files
        return "is not a flash card"
