import subprocess


class ADBManager(object):

    def adb_shell(self, device_id):
        # connect to specific device by it device_id
        p = subprocess.Popen('adb -s {} shell ls -la'.format(device_id), shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        result = p.stdout.read().decode("utf-8")
        if result:
            adb_info = result.split("\n") if "\n" in result else result
        else:
            adb_info = "This device doesn't support adb"
        return adb_info
