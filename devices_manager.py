import threading
import time
import json
import logging

from managers.flash_card_manager import FlashCardManager
from managers.adb_manager import ADBManager


FORMAT = '%(asctime)s,%(msecs)03d %(name)s[%(filename)s] %(levelname)s %(message)s'
logging.basicConfig(format=FORMAT)
logger = logging.getLogger('paramiko')
logger.setLevel(logging.WARNING)


class DevicesManager(object):
    __instance = None

    def __new__(cls):
        if DevicesManager.__instance is None:
            DevicesManager.__instance = object.__new__(cls)
            DevicesManager.__instance.devices_info = {}
        return DevicesManager.__instance

    def manage_devices(self, devices):
        device_info = {
            "device_info": [dict(device.items()) for device in devices]
        }
        threading.Thread(target=self.detect_additional_info, args=(devices[0], device_info)).start()

    def detect_additional_info(self, usb_device, device_info):
        time.sleep(1)
        device_info["adb_shell"] = ADBManager().adb_shell(usb_device.get("ID_SERIAL_SHORT"))

        time.sleep(1)
        device_info["flash_card_files"] = FlashCardManager(usb_device.get("DEVPATH")).mount_and_read()

        # from managers.mtp_manager import MTPManager
        # MTPManager().mtp_connect()

        print('{} connected'.format(usb_device.get("ID_SERIAL")))
        print(json.dumps(device_info, indent=2))

        # TODO: add checking, if device was connected in session early,
        #       and append data to existed info
        self.devices_info[usb_device.get("ID_SERIAL_SHORT")] = device_info

    def printing(msg):
        print(msg)
        logger.info(msg)