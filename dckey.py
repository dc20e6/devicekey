#!/usr/bin/env python3
import platform
import sys
import pyudev

from devices_manager import DevicesManager


if __name__ == "__main__":
    if sys.version_info.major < 3:
        sys.exit('DeviceKey supports only Python3. Rerun application in Python3 environment.')
    elif platform.system().lower() != "linux":
        sys.exit('This script can run only on a linux platform')

    context = pyudev.Context()
    monitor = pyudev.Monitor.from_netlink(context)
    monitor.filter_by(subsystem='usb')

    # Monitoring usb devices which add in real-time
    for device in iter(monitor.poll, None):
        if device.action == 'add' and device.get("DEVTYPE") == "usb_device":

            # Get all device routes by one usb port
            connected_devices_in_port = [dev for dev in context.list_devices(subsystem='usb')
                                         if dev.get('DEVPATH').startswith(device.get("DEVPATH"))]
            DevicesManager().manage_devices(connected_devices_in_port)
