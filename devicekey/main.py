from devicekey.core.usb import USBDevices
# from devicekey.core.pyudev import Pyudev
# from devicekey.core.pymtp import Pymtp
from devicekey.core.lsusb import Lsusb
import platform
import os
from elevate import elevate


class DeviceKey(object):

    def start(self):
        self.monitoring()

    def monitoring(self):
        if self.detect_root_permissions():
            USBDevices.devices_info()
        if platform.system().lower() == "linux":
            Pyudev.devices_info()
        Pymtp.devices_info()
        Lsusb.devices_info()

    def detect_root_permissions(self):
        if not self.is_root():
            print("for running pyusb analyzator, you need root privileges")
            elevate(graphical=False)
        return self.is_root()

    def is_root(self):
        return os.getuid() == 0
