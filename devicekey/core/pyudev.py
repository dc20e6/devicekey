import pyudev

context = pyudev.Context()
monitor = pyudev.Monitor.from_netlink(context)
monitor.filter_by(subsystem='usb')
import json


class Pyudev(object):

    @staticmethod
    def devices_info():
        print('\n\n<PY_U_DEV USB INFORMATION>')

        devices_info = []

        for device in context.list_devices(subsystem='usb'):
            if not device or device.get("DEVTYPE") != "usb_device" or device.get("DRIVER") != "usb":
                continue

            device_info = {}
            for key in list(device.keys()):
                device_info[key] = device.get(key)
            devices_info.append(device_info)
        print(json.dumps(devices_info, indent=2))
