from pyudev import Context, Monitor
import pyudev
import json
import psutil
import os
import time
import threading


class PyudevMonitoring(object):
    def __init__(self):
        self.context = pyudev.Context()
        self.monitor = pyudev.Monitor.from_netlink(self.context)
        self.monitor.filter_by(subsystem='usb')

    def start_monitoring(self):
        for device in iter(self.monitor.poll, None):
            if device.action == 'add':
                print('{} connected'.format(device))
                self.device_info(device.get('DEVPATH'))

    def device_info(self, dev_path, usb_removable_device=None):
        for device in self.context.list_devices(subsystem='usb'):
            usb_dev_path = device.get('DEVPATH')
            if not dev_path.startswith(usb_dev_path):
                continue

            try:
                if len(usb_dev_path) > len(usb_removable_device.get('DEVPATH')):
                    usb_removable_device = device
            except AttributeError:
                usb_removable_device = device
                continue

            device_info = {}
            for key in list(usb_removable_device.keys()):
                device_info[key] = usb_removable_device.get(key)

            print(json.dumps(device_info, indent=2) + "\n\n")
            threading.Thread(target=self.monitoring_thread,
                             args=(usb_removable_device.get("DEVPATH"), device_info)).start()

    def monitoring_thread(self, device_path, device_info):
        print("Detect if {} is usb storage ...\n\n".format(device_path))
        time.sleep(3)
        removable = [dev for dev in self.context.list_devices(subsystem='block', DEVTYPE='disk') if dev.attributes.asstring('removable') == "1"]
        if removable and [dev for dev in removable if dev.get("DEVPATH").startswith(device_path)]:
            for device in removable:
                if device.get("DEVPATH").startswith(device_path):
                    partitions = [device.device_node for device in self.context.list_devices(subsystem='block', DEVTYPE='partition', parent=device)]
                    print("All removable partitions: {}".format(", ".join(partitions)))
                    print("Mounted removable partitions:")
                    files = []
                    for p in psutil.disk_partitions():
                        if p.device in partitions:
                            print("  {}: {}".format(p.device, p.mountpoint))
                            files.append(os.listdir(p.mountpoint))
                    print("{} -- files --> {}".format(json.dumps(device_path, indent=2), json.dumps(files, indent=2)) )
        else:
            print("{} is not a usb storage".format(device_path))
