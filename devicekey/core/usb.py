import usb
import json


class USBDevices(object):

    def all_devices(self):
        devices = self.devices_info()
        for device in devices:
            print(device)

    @staticmethod
    def devices_info():
        devices = usb.core.find(find_all=True)
        usb_device = USBDevices()

        print('\n\n<PY_USB USB INFORMATION>')

        devices_info = {
            "devices": [
                {
                    "device_info": usb_device.device_info(device),
                    "configurations": [
                        {
                            "configuration": usb_device.device_configuration(device, configuration),
                            "interfaces": [
                                {
                                    "interface": usb_device.device_interface(device, interface),
                                    "endpoints": [
                                        {
                                            "endpoint": usb_device.device_endpoint(device, endpoint)
                                        }
                                        for endpoint in interface.endpoints()
                                    ]
                                }
                                for interface in configuration.interfaces() if
                                usb.util.get_string(device, interface.iInterface)
                            ]
                        }
                        for configuration in device.configurations() if
                        usb.util.get_string(device, configuration.iConfiguration)
                    ]
                }
                for device in devices
            ]
        }
        print(json.dumps(devices_info, indent=2))
        return devices_info

    def device_info(self, device):
        manufacturer = usb.util.get_string(device, device.iManufacturer)
        product = usb.util.get_string(device, device.iProduct)
        serialnumber = str(usb.util.get_string(device, device.iSerialNumber))[:-5] + '****'
        return {
            'manufacturer': manufacturer if manufacturer else 'N/A',
            'product': product if product else 'N/A',
            'serialnumber': serialnumber if serialnumber else 'N/A'
        }

    def device_configuration(self, device, configiration):
        return {
            'configuration': usb.util.get_string(device, configiration.iConfiguration),
            'maxpower': usb.util.get_string(device, configiration.bMaxPower) if not usb.core.USBError else 'N/A'
        }

    def device_interface(self, device, interface):
        return {
            'interface': usb.util.get_string(device, interface.iInterface),
            'interfaceclass': usb.util.get_string(device, interface.bInterfaceClass) if not usb.core.USBError else 'N/A'
        }

    def device_endpoint(self, device, endpoint):
        return {
            'descriptortype': usb.util.get_string(device, endpoint.bDescriptorType),
            'endpointaddress': usb.util.get_string(device, endpoint.bEndpointAddress) if not usb.core.USBError else 'N/A'
        }
